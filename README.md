# Brazil portable phones - REST API
## REST API

* Получение списка всех телефонных операторов страны:


    GET /v1.0/operators
    
* Проверка телефонного номера (номер передаётся в международном формате, начиная с кода страны):


    GET /v1.0/phone_number/<phone_number>
    
* Изменение информации о портированном номере: 


    PUT /v1.0/phone_number/<phone_number>
    
payload application/json :

    {
	    "operator_id" : 1459,
	    "date" : "2017-08-29 00:00:00"
    }

* Удаление информации о портированном номере:


    DELETE /v1.0/phone_number/<phone_number>

## Install
Создаём окружение:

1. (опционально) Устанавлваем pip и virtualenv


    sudo apt-get install python3-pip    
    sudo apt-get install python-virtualenv

2. Создаём виртуальное окружение


    virtualenv -p python3 .env

3. Активируем виртуальное окружение и устанавливаем зависимости


    source .env/bin/activate
    pip install -e .
    
4. Создать базу данных из [схемы](db/schema.sql) и прописать строку подключения в local.cfg

## Запуск сервера REST API


    source .env/bin/activate
    PORTABLE_REST_SETTINGS=../local.cfg FLASK_APP=rest flask run
    
## Запуск тестов    
    
    source .env/bin/activate
    PORTABLE_REST_SETTINGS=../tests.cfg python setup.py test
    
## Запуск импорта
Скрипт запуска `scripts/import.py`. Пример:

    source .env/bin/activate
    python scripts/import.py -c local.cfg --portable data/portabilidade.csv
           
