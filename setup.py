from setuptools import setup

setup(
    name='portable_phones',
    packages=['rest'],
    install_requires=[
        'flask==0.12.2',
        'flask-negotiate==0.1.0',
        'psycopg2==2.7.3',
        'pylint==1.7.2',
        'requests==2.18.4'
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ]
)