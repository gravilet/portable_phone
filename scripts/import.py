"""
import module
"""

import argparse
import csv
import os
import re
from io import StringIO
from logging import DEBUG

from psycopg2._psycopg import IntegrityError

from rest.database import get_cursor, set_connection_string
from utils.utils import get_logger

# TODO: create lock-file for prevent twice running import any files
# TODO: add logging level into config (for output 'info' not only 'warning+error')

# TODO: add log-history table about import files - name, date etc.


LOG = get_logger('import', level=DEBUG)

IMPORT_LOCK = 'import'


def init_db(filename):
    """ init db from config file """
    cfg = {}
    # config parsing as in flask.config.Config#from_pyfile
    with open(filename, mode='rb') as config_file:
        exec(compile(config_file.read(), filename, 'exec'), cfg)
    set_connection_string(cfg['DATABASE'])


def main():
    """
    import base data -> operators (and fill their codes), prefixes, portable numbers
    :return:
    """
    parser = argparse.ArgumentParser(description='Brazil')

    parser.add_argument('-c', '--config',
                        help='config file')

    parser.add_argument('-o', '--operator',
                        help='operators file')

    parser.add_argument('-x', '--prefix',
                        help='prefix file')

    parser.add_argument('-p', '--portable',
                        help='portable numbers file')

    parser.add_argument('-d', '--debug', action='store_true',
                        help='enable debug')

    args = parser.parse_args()

    if args.config is None or not os.path.isfile(args.config):
        raise ValueError('Config file %s not found' % args.config)

    if args.operator is None and args.prefix is None and args.portable is None:
        parser.error('Set "expression" OR --file path/to/file')

    LOG.info('START')
    init_db(args.config)
    with get_cursor(commit=True) as cursor:
        try:
            cursor.query('insert into lock (lock_type) values (%s)', (IMPORT_LOCK,))
            cursor.commit()
        except IntegrityError:
            raise ValueError('Another import not finished yet')

        import_task = Import(cursor, args)
        if args.operator is not None:
            import_task.run_import_operators()
            #TODO: ask 'continue?' before next import - because we can output some errors
        if args.prefix is not None:
            import_task.run_import_prefix()
        if args.portable is not None:
            import_task.run_import_portable()

        cursor.query('delete from lock t where t.lock_type = %s', (IMPORT_LOCK,))

    LOG.info('FINISH')


class Import(object):
    """
    Class to import database objects
    """
    def __init__(self, cursor, p_args):
        self.cursor = cursor
        self.args = p_args
        self.inserts = []
        self.updates = {}   # 0 - portable_id, 1 - code, 2 - porting_date
        self.phone_number_to_date = {}

        self.dict_codes = {}
        self.counts = {'insert': 0, 'update': 0}

    def run_import_operators(self):
        """
        import operators (and fill their codes)
        :return:
        """
        #TODO: need to LOCK table 'operators' - we select from it, then insert => nobody another can insert in that time
        dict_operators = {}
        for row in self.cursor.query("select operator_id, operator_fullname from operators").fetchall():
            dict_operators[row['operator_fullname']] = row['operator_id']

        country_name = "Brazil"
        dict_types = {
            "Fixo": "Fixed",
            "Celular": "Mobile",
            "Rádio": "Radio"
        }

        if not os.path.isfile(self.args.operator):
            raise ValueError('Operators file %s not found' % self.args.operator)
        with open(self.args.operator) as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                # [0] - code, [1] - operator raw name
                #TODO : Fixo|Celular|Rádio - to global var
                #TODO : Brazil - to global var too
                matches = re.search('^(?P<name>.*)\\s+(?P<type>Fixo|Celular|Rádio)$', row[1])
                if matches is None:
                    LOG.warning('string coudn''t be parsed: %s', row[1])
                    continue
                groups = matches.groupdict()
                groups['name'] = groups['name'].strip(' -')
                full_name = country_name + " " + dict_types[groups['type']] + " " + groups['name']

                # check 'code' - is it exist in table 'operator_codes'
                exist = self.cursor.query(
                    'select t1.operator_fullname as exist from operators t1, operator_codes t2 '
                    'where t2.code = %s and t2.operator_id = t1.operator_id',
                    (row[0],)
                ).fetchone()
                if exist is not None:
                    if exist['exist'] == full_name:
                        LOG.info('string already exist: "%s"', row)
                    else:
                        LOG.error('string already exist: %s '
                                  'and for code="%s" name in base="%s" differ from name in file="%s"',
                                  row, row[0], exist['exist'], full_name)
                    continue

                if full_name not in dict_operators:
                    # insert into operators
                    new_id = self.cursor.query(
                        'insert into operators (operator_name, operator_type, operator_fullname) '
                        'values (%s, %s, %s) returning operator_id',
                        (groups['name'], groups['type'], full_name)).fetchone()['operator_id']
                    dict_operators[full_name] = new_id

                # insert into operator_codes
                #TODO: add unique key to column pair operator_id+code ???
                self.cursor.query(
                    'insert into operator_codes (operator_id, code) values (%s, %s) returning operator_id',
                    (dict_operators[full_name], row[0]))

        #TODO: need or not 'close cursor'??? -- http://initd.org/psycopg/docs/usage.html
        self.cursor.commit()
        # TODO: need or not 'close connection'??? conn.close() -- http://initd.org/psycopg/docs/usage.html

    def fetch_pair(self, key, value, table_name, key_type=str):
        """
        create dictionary key_column->value_columns on some table
        ACHTUNG! can be sql-injection
        :param key:
        :param value:
        :param table_name:
        :return:
        """
        pairs = {}
        for row in self.cursor.query("select %s, %s from %s" % (key, value, table_name)).fetchall():
            key_ = key_type.__call__(row[key])
            pairs[key_] = row[value]
        return pairs

    def run_import_prefix(self):
        """
        import prefixes
        :return:
        """
        if not os.path.isfile(self.args.prefix):
            raise ValueError('Prefixes file %s not found' % self.args.operator)

        dict_codes = self.fetch_pair('code', 'operator_id', 'operator_codes')
        # Create file-like object to write processed data into. Then quick insert from it to table 'prefix'
        sql_copy = ''
        # TODO: need to LOCK table 'prefix' - we select from it, then insert => nobody another can insert in that time
        dict_prefix = self.fetch_pair('prefix', 'operator_id', 'prefix_anatel')
        with open(self.args.prefix) as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                # [0] - prefix, [1] - operator code
                prefix = row[0]
                code = row[1]
                if code not in dict_codes:
                    LOG.error('wrong code (doesn''t present in system) in string: "%s"', row)
                    continue
                if prefix in dict_prefix:
                    if dict_prefix[prefix] == dict_codes[code]:
                        LOG.info('string already exist: "%s"', row)
                    else:
                        LOG.error('string already exist: %s '
                                  'and for prefix="%s" operator in base="%s" differ from operator in file="%s"',
                                  row, prefix, dict_prefix[prefix], dict_codes[code])
                else:
                    sql_copy += '%s;%s\n' % (prefix, dict_codes[code])
                    dict_prefix[prefix] = dict_codes[code]
        file = StringIO(sql_copy)
        self.cursor.copy_from(file, 'prefix_anatel', columns=('prefix', 'operator_id'), sep=';')
        self.cursor.commit()

    def run_import_portable(self):
        """
        import portable phone numbers
        :return:
        """
        if not os.path.isfile(self.args.portable):
            raise ValueError('Portable numbers file %s not found' % self.args.operator)

        # TODO : lock table for dict_codes ???
        # TODO: need to LOCK table 'portable numbers' :
        # TODO: we select from it, then insert => nobody another can insert in that time
        cnt = self.cursor.query('select count(*) cnt from portable_number_import').fetchone()['cnt']
        if cnt > 0:
            raise ValueError('table portable_number_import is not empty!')

        import_index_name = 'portable_number_import_phone_number_index'
        self.cursor.query('DROP INDEX IF EXISTS %s' % import_index_name)
        LOG.info('Import portables: import table index dropped')

        LOG.info('Import portables: copy from START')
        with open(self.args.portable, mode='r') as file:
            self.cursor.copy_from(file, 'portable_number_import',
                                  columns=('porting_id', 'phone_number', 'code', 'porting_date'), sep=';')
        self.cursor.commit()
        LOG.info('Import portables: copy from FINISH - completed and commited')

        LOG.info('Import portables: import table index - START - create')
        self.cursor.query('CREATE INDEX %s ON portable_number_import (phone_number)' % import_index_name)
        self.cursor.commit()
        LOG.info('Import portables: import table index - FINISH - created')

        # get ambiguously phone numbers
        LOG.info('Import portables: get ambiguously phone numbers')
        ambiguously_phones = self.cursor.query('''
select * from portable_number_import t3
where t3.phone_number in (
	SELECT t2.phone_number
	FROM (
				 SELECT
					 t1.phone_number,
					 count(*) cnt
				 FROM portable_number_import t1
				 GROUP BY t1.phone_number
			 ) t2
	WHERE t2.cnt > 1
)''').fetchall()
        # 'phone': { 'best_date': datetime, 'best_id': 1234, 'ids': [1234, 5678] }
        LOG.info('Import portables: search bad phones in %s ambiguously phones', len(ambiguously_phones))
        phones = {}
        for item in ambiguously_phones:
            if item['phone_number'] in phones:
                if phones[item['phone_number']]['best_date'] < item['porting_date']:
                    phones[item['phone_number']]['ids'].append(phones[item['phone_number']]['best_id'])
                    phones[item['phone_number']]['best_date'] = item['porting_date']
                    phones[item['phone_number']]['best_id'] = item['porting_id']
                else:
                    phones[item['phone_number']]['ids'].append(item['porting_id'])
            else:
                phones[item['phone_number']] = {'best_date': item['porting_date'],
                                                'best_id': item['porting_id'],
                                                'ids': []}
        LOG.info('Import portables: %s unique phones found', len(phones))
        # collect BAD phones (bad duplicates)
        bad_phones = []
        for item in phones:
            bad_phones += phones[item]['ids']
        LOG.info('Import portables: %s bad phones found', len(bad_phones))
        # delete bad_phones from import-table
        bad_phones_len = len(bad_phones)
        if bad_phones_len > 0:
            LOG.info('Import portables: deleting bad phones')
            self.cursor.query('delete from portable_number_import t '
                              'where t.porting_id in %s '
                              'and t.phone_number in %s',
                              (tuple(bad_phones), tuple(phones.keys())))
            self.cursor.commit()

        cnt = self.cursor.query('select count(*) cnt from portable_number_import').fetchone()['cnt']

        offset = 0
        step = 100000
        while offset < cnt:
            self.cursor.query('''
INSERT INTO portable_number as d (porting_id, phone_number, portable_operator_id, porting_date)
		select imp.porting_id, imp.phone_number, c.operator_id, imp.porting_date from
			portable_number_import imp,
			operator_codes as c
		where imp.code = c.code
			offset %s limit %s
ON CONFLICT (phone_number) do
UPDATE SET
	porting_id = excluded.porting_id,
	phone_number = excluded.phone_number,
	portable_operator_id = excluded.portable_operator_id,
	porting_date = excluded.porting_date
WHERE d.porting_date < excluded.porting_date''' % (offset, step))
            self.cursor.commit()
            offset += step
            LOG.info('Import portables: merged %s / %s', min(offset, cnt), cnt)

        self.cursor.query('TRUNCATE TABLE portable_number_import CONTINUE IDENTITY')
        LOG.info('Import portables: import table truncated')


if __name__ == '__main__':
    main()
