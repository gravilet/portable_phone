"""
Module for imitate high load on REST API
"""


import argparse
from logging import INFO
from multiprocessing.pool import ThreadPool
import random
import requests
from utils.utils import get_logger

LOG = get_logger('highload', level=INFO)

ANSWERS = {200: 0, 204: 0, 400: 0, 404: 0, 500: 0}


def main():
    """
    run 50%/50% GET and PUT phone_number for random portable phone number
    :return:
    """
    parser = argparse.ArgumentParser(description='highload')

    parser.add_argument('-t', '--threads_num',
                        help='number of threads', required=True, type=int)

    parser.add_argument('-n', '--requests_num',
                        help='number of requests', required=True, type=int)

    args = parser.parse_args()
    # print(args)

    # make the Pool of workers
    pool = ThreadPool(args.threads_num)

    phones_array = []
    for _ in range(0, args.requests_num):
        phones_array.append({
            'request_method': 'GET' if random.random() < 0.5 else 'PUT',
            'request_path':
                'http://127.0.0.1:5000/v1.0/phone_number/55%s' % int(random.uniform(0000000000, 9999999999))
        })

    # open the urls in their own threads
    # and return the results
    pool.map(touch_web, phones_array)

    # close the pool and wait for the work to finish
    pool.close()
    pool.join()

    print(ANSWERS)


def touch_web(param):
    """
    run someone rest-api function with app.py
    now only for 'GET phone number' and 'GET phone number' ('get_phone_number' and 'update_phone_number')
    :param param: input parameters
    :type param: dict
    :return
    """
    url = param['request_path']
    if param['request_method'] == 'PUT':
        # 1459 - yeah, hardcode => 'Brazil Fixed TELECALL'
        json = {
            "operator_id": 1459,
            "date": "2017-08-28 20:00:00"
        }
        response = requests.put(url, json=json)
        ANSWERS[response.status_code] += 1
    else:
        response = requests.get(url)
        ANSWERS[response.status_code] += 1


if __name__ == '__main__':
    main()
