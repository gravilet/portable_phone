"""
module for work with database
"""

from urllib.parse import urlparse
from contextlib import contextmanager
import psycopg2
import psycopg2.extras
from psycopg2.pool import ThreadedConnectionPool

CONNECTION_STRING = None
DB_CONTEXT = {}


class Cursor(object):
    """
    :param cursor: sql cursor
    :type cursor: RealDictCursor
    """
    def __init__(self, connection) -> None:
        """

        :param connection:
        """
        self.cursor = connection.cursor(
            cursor_factory=psycopg2.extras.RealDictCursor)

    def query(self, query, bind_vars=None):
        """ query """
        self.cursor.execute(query, bind_vars)
        return self

    def fetchone(self):
        """ fetch one row """
        return self.cursor.fetchone()

    def fetchall(self):
        """ fetch all rows """
        return self.cursor.fetchall()

    def copy_from(self, *args, **kwargs):
        """ copy from command """
        self.cursor.copy_from(*args, **kwargs)
        return self

    def commit(self):
        """ commit  transaction """
        self.cursor.connection.commit()
        return self

    def close(self):
        """ close cursor """
        self.cursor.close()

    @property
    def rowcount(self):
        """ get affected rows count """
        return self.cursor.rowcount


def set_connection_string(string):
    """ Set db connection string """
    global CONNECTION_STRING
    CONNECTION_STRING = string


def register_db(app):
    """ set db connection string from Flask application config"""
    set_connection_string(app.config['DATABASE'])


def get_pool():
    """ get db pool """
    global DB_CONTEXT
    if 'rest_db_pool' not in DB_CONTEXT:
        url = urlparse(CONNECTION_STRING)
        DB_CONTEXT['rest_db_pool'] = ThreadedConnectionPool(1, 10,
                                                            database=url.path[1:],
                                                            user=url.username,
                                                            password=url.password,
                                                            host=url.hostname,
                                                            port=url.port)
    return DB_CONTEXT['rest_db_pool']


@contextmanager
def get_connection():
    """ get db connection """
    connection = None
    try:
        connection = get_pool().getconn()
        connection.autocommit = False
        yield connection
    finally:
        if connection is not None:
            get_pool().putconn(connection)


@contextmanager
def get_cursor(commit=False):
    """ get db cursor """
    with get_connection() as connection:
        cursor = Cursor(connection)
        try:
            yield cursor
            if commit:
                connection.commit()
        finally:
            cursor.close()
