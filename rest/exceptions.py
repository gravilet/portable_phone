"""
exception module
"""


class ApiException(Exception):
    """ self-made exception class """
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        """
        represent exception as a dict
        :return:
        """
        result = dict(self.payload or ())
        result['message'] = self.message
        return result


def bad_request(message):
    """ raise ApiException with 400 http code """
    return ApiException(message, status_code=400)
