"""
portable phone numbers module
"""

import datetime
import re
from rest.database import get_cursor
from rest.exceptions import bad_request
from utils.utils import get_logger, date_to_str

LOG = get_logger('phone_numbers')


def purifier(phone_number):
    """ purify phone number from country code (55)
    :param phone_number:
    :return:
    """
    # TODO: purge hardcode - country code '55'
    country_code = '55'  # Brazil
    matches = re.search('^%s(?P<number_pure>\\d+)$' % country_code, phone_number)  # \d+ => one or more digit

    if matches is None:
        LOG.warning('bad phone number format: %s', phone_number)
        raise bad_request('bad phone number format: %s' % phone_number)
    groups = matches.groupdict()
    number_pure = groups['number_pure']
    return number_pure


def get_entity(phone_number):
    """
    Проверку телефонного номера (номер передаётся в международном формате, начиная с кода страны).
    В ответе должна содержаться информация о валидности номера, какому оператору номер принадлежит,
    является ли номер в данный момент портированным и когда он был портирован.
    :return:
    """
    number_pure = purifier(phone_number)

    # TODO : write in manual that miss prefix is more important than poratble number existence
    result = {'is_valid': False, 'operator': None, 'is_porting': False, 'porting_date': None}

    # TODO: to select MAXLEN prefix or not to select ??? (analize table) - now we expext only one row
    with get_cursor(commit=True) as cursor:
        # select ... like c.prefix||'%' - without using index => cutting prefix and "prefix in (....)"
        prefix_cutted = [number_pure]
        for subst_idx in range(1, len(number_pure)):
            prefix_cutted.append(number_pure[:subst_idx])
        row = cursor.query('select p.operator_id from prefix_anatel p where p.prefix in %s',
                           (tuple(prefix_cutted),)
                          ).fetchone()
        # 'fetchone' because we expect only one prefix - feature of input data

        operator_id = None
        if row is not None:
            result['is_valid'] = True
            operator_id = row['operator_id']    # default operator by prefix

        portable = cursor.query(
            'select portable_operator_id, porting_date from portable_number t where t.phone_number = %s',
            (number_pure,)
        ).fetchone()

        if portable is not None:
            result['porting_date'] = date_to_str(portable['porting_date'])
            result['is_porting'] = True
            operator_id = portable['portable_operator_id']

        if operator_id is not None:
            result['operator'] = cursor.query(
                'select operator_id, operator_name as name, operator_type as type, operator_fullname as fullname '
                'from operators where operator_id = %s',
                (operator_id,)
            ).fetchone()

        return result


def porting(phone_number, content):
    """
    Изменение информации о портированном номере.
    на входе телефонный номер, ID нового оператора (code from operator_codes OR operator_id) и дату портирования
    :param phone_number:
    :return:
    """
    number_pure = purifier(phone_number)

    if content['date'] is None or not isinstance(content['date'], datetime.datetime):
        raise bad_request('Wrong date format')

    with get_cursor(commit=True) as cursor:
        if 'code' in content:
            pretender_operator = cursor.query(
                'select operator_id from operator_codes where code = %s',
                (content['code'],)
            ).fetchone()
            if pretender_operator is None:
                raise bad_request('Code %s not found' % content['code'])
            if 'operator_id' in content and content['operator_id'] != pretender_operator['operator_id']:
                raise bad_request('Code %s not related with operator %s' % (content['code'], content['operator_id'],))
            content['operator_id'] = pretender_operator['operator_id']
        elif 'operator_id' in content:
            row = cursor.query(
                'select operator_id from operators where operator_id = %s',
                (content['operator_id'],)
            ).fetchone()
            if row is None:
                raise bad_request('Operator %s not found' % content['operator_id'])
        else:
            raise bad_request('Code or operator required')

        portable = cursor.query(
            'select * from portable_number t where t.phone_number = %s',
            (number_pure,)
        ).fetchone()

        if portable is None:    # insert
            cursor.query('insert into portable_number (phone_number, portable_operator_id, porting_date) '
                         'values (%s, %s, %s)',
                         (number_pure, content['operator_id'], content['date']))
        elif portable['porting_date'] < content['date']:                   # update
            cursor.query('update portable_number set portable_operator_id = %s, porting_date = %s, porting_id = NULL '
                         'where portable_number_id = %s',
                         (content['operator_id'], content['date'], portable['portable_number_id']))
        else:
            raise bad_request('Date is earlier than the one presented in the database')
    # no 'retrun' (no raise => normal ending)


def unporting(phone_number):
    """
    Удаление информации о портированном номере.
    (т.е. возврат номера к оригинальному оператору,
    которому пренадлежит номерная ёмкость-префикс, покрывающий данный номер)
    на входе телефонный номер
    :param phone_number:
    :return:
    """
    #TODO: "возврат номера к оригинальному оператору" - ...
    #TODO: вообще-то по префиксам может оказаться, что номер невалиден - да и чёрт с ним
    number_pure = purifier(phone_number)

    with get_cursor(commit=True) as cursor:
        cursor.query('delete from portable_number where phone_number = %s', (number_pure,))
        if cursor.rowcount == 0:
            raise bad_request('Portable number not found')
    # no 'retrun' (no raise => normal ending)
