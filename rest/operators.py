"""
operators module
"""
from rest.database import get_cursor


def get_list():
    """ get all operators entities """
    with get_cursor() as cursor:
        return cursor.query('select t1.*, '
                            'json_agg(json_build_object( '
                            '\'operator_code_id\', t2.operator_code_id, '
                            '\'code\', t2.code '
                            ')) as codes  '
                            'from operators t1, operator_codes t2 '
                            'where t1.operator_id = t2.operator_id '
                            'group by t1.operator_id'
                           ).fetchall()
