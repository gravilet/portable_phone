"""
main REST API module
"""
import datetime

from flask import jsonify, request, Blueprint
from flask_negotiate import consumes
from werkzeug.exceptions import BadRequest

from rest import phone_numbers
from rest.exceptions import ApiException, bad_request
from rest.log import log
from rest.operators import get_list

bp = Blueprint('rest', __name__)


@bp.route("/")
def hello():
    """ hello """
    return "Hello World!"


# OPERATORS
@bp.route("/operators")       # GET by default
def operators():
    """ get all operators """
    return jsonify(get_list())


# PORTABLE NUMBERS
#TODO for GET/PUT port.numb. return "system is busy with import, wait a little time" when import port.num. is work
@bp.route("/phone_number/<phone_number>", methods=['GET'])
def get_phone_number(phone_number):
    """ get number entity by phone number (starts by country code) """
    phone_info_dict = phone_numbers.get_entity(phone_number)
    return jsonify(phone_info_dict)


@bp.route("/phone_number/<phone_number>", methods=['PUT'])
@consumes('application/json')           # support only json - otherwise '415 Unsupported Media Type'
def update_phone_number(phone_number):
    """ change operator id for phone number """
    content = request.get_json()
    # check request
    if 'code' in content and 'operator_id' in content:
        raise bad_request('Only code OR operator_id are supported')
    if 'date' not in content or ('code' not in content and 'operator_id' not in content):
        raise bad_request('Unsupported set of fields')
    try:
        content['date'] = datetime.datetime.strptime(content['date'], '%Y-%m-%d %H:%M:%S')
    except ValueError as eee:
        raise bad_request('Bad date format. Use ''Y-m-d H:M:S'' - %s' % eee)
    phone_numbers.porting(phone_number, content)
    return '', 204  # 204 - No Content - The server has fulfilled the request but does not need to return an entity-body


@bp.route("/phone_number/<phone_number>", methods=['DELETE'])
def delete_phone_number(phone_number):
    """ delete portability """
    phone_numbers.unporting(phone_number)
    return '', 204  # 204 - No Content - The server has fulfilled the request but does not need to return an entity-body


@bp.errorhandler(ApiException)
def handle_api_exception(error):
    """ handle ApiException """
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# use HTTPException instead BadRequest when
# https://github.com/pallets/flask/issues/941 issue fixed (expected in Flask v.0.12.3)
@bp.errorhandler(BadRequest)
def handle_http_exception(error):
    """ handle http exception """
    response = jsonify({'message' : error.description, 'code' : error.code})
    response.status_code = error.code
    return response


@bp.after_request
def log_request_info(response):
    """ logging http requests """
    log(request, response)
    return response
