"""
init application
"""
from flask import Flask

from rest import rest
from rest.database import register_db
from rest.rest import bp


def create_app():
    """
    Create REST application
    :return:
    """
    application = Flask(__name__)

    application.config.update(dict(
        DATABASE=' use config file',
        DEBUG=True,
    ))
    application.config.from_envvar('PORTABLE_REST_SETTINGS', silent=True)
    application.register_blueprint(bp, url_prefix='/v1.0')
    register_db(application)
    return application

_ = create_app()
