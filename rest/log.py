"""
log module
"""

from flask import json
from rest.database import get_cursor


def log(request, response):
    """
    log rest api to database
    :param request:
    :param response:
    :return:
    """
    with get_cursor(commit=True) as cursor:
        operator_id = None
        if request.endpoint == 'get_phone_number' and response.status_code == 200:
            my_json = response.data.decode('utf8').replace("'", '"')
            data = json.loads(my_json)
            # TODO: write test for empty operator + may be 'if' based on is_valid, not operator_id ???
            if data['operator'] is not None:
                operator_id = data['operator']['operator_id']
        cursor.query('insert into rest_api_log '
                     '(request_date, client_ip, request_method, request_path, request_data, operator_id, status_code) '
                     'VALUES ( '
                     'now(), %s, %s, %s, %s, %s, %s)',
                     (request.environ['REMOTE_ADDR'],
                      request.environ['REQUEST_METHOD'][:10], request.environ['PATH_INFO'][:255],
                      request.data.decode('utf8')[:255],
                      operator_id,
                      response.status_code))
