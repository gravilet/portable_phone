create table operator_codes
(
	code integer not null,
	operator_id integer not null,
	operator_code_id serial not null
		constraint operators_code_id_pk
			primary key
)
;

create unique index operator_codes_code_uindex
	on operator_codes (code)
;

create table prefix_anatel
(
	prefix varchar(20) not null,
	operator_id integer not null,
	prefix_anatel_id serial not null
		constraint prefix_anatel_prefix_anatel_id_pk
			primary key
)
;

create unique index prefix_anatel_prefix_uindex
	on prefix_anatel (prefix)
;

create table portable_number
(
	portable_number_id serial not null
		constraint portable_number_portable_number_id_pk
			primary key,
	porting_id integer,
	phone_number varchar(20) not null,
	portable_operator_id integer not null,
	porting_date timestamp not null
)
;

create unique index portable_number_phone_number_uindex
	on portable_number (phone_number)
;

create table operators
(
	operator_id serial not null
		constraint operators_pkey
			primary key,
	operator_name varchar(255) not null,
	operator_type varchar(10) not null,
	operator_fullname varchar(300) not null
)
;

alter table operator_codes
	add constraint operator_codes_operators_operator_id_fk
		foreign key (operator_id) references operators
;

alter table prefix_anatel
	add constraint prefix_anatel_operators_operator_id_fk
		foreign key (operator_id) references operators
;

alter table portable_number
	add constraint portable_number_operators_operator_id_fk
		foreign key (portable_operator_id) references operators
;

create table rest_api_log
(
	request_date timestamp,
	client_ip varchar(15),
	request_method varchar(10),
	request_path varchar(255),
	request_data varchar(255),
	operator_id integer,
	status_code integer
)
;

create index rest_api_log_request_date_index
	on rest_api_log (request_date)
;

comment on column rest_api_log.status_code is 'http status code'
;

create table lock
(
	lock_type varchar(255) not null
		constraint lock_lock_type_pk
			primary key
)
;

create table portable_number_import
(
	porting_id integer,
	phone_number varchar(20),
	porting_date timestamp,
	code integer
)
;

create index portable_number_import_phone_number_index
	on portable_number_import (phone_number)
;

