"""
test REST API
"""
import datetime
import json
import pytest
from rest import create_app
from rest.database import get_cursor
from utils.utils import date_to_str


@pytest.fixture
def app(request):
    app = create_app()

    with app.app_context():
        yield app


@pytest.fixture
def client(request, app):
    client = app.test_client()
    return client


def to_json(data):
    return json.loads(data.decode('utf8'))


def test_get_operators(client):
    resp = client.get('/v1.0/operators')
    assert resp.status_code == 200
    json_data = to_json(resp.data)
    with get_cursor() as cursor:
        cnt = cursor.query('select count(*) cnt from operators').fetchone()['cnt']
    assert len(json_data) == cnt


# test 'invalid' (by prefix) phone
def test_get_phone_number__invalid(client):
    with get_cursor(commit=False) as cursor:
        # минимальный среди _всех_ префиксов
        min_prefix = cursor.query('select min(t.prefix) min_prefix from prefix_anatel t').fetchone()['min_prefix']
        len_ = len(min_prefix)
        # если убрать у минимального префикса одну цифирь, то наверняка получи "невалидный по префиксу" номер
        resp = client.get('/v1.0/phone_number/55%s' % min_prefix[:len_ - 1])
        assert resp.status_code == 200
        json_data = to_json(resp.data)
        assert json_data['is_valid'] is False


def test_phone_number__complex(client):
    """
    don't use on real base
    :param client:
    :return:
    """
    with get_cursor(commit=False) as cursor:
        # первый попавшийся оператор и _его_ минимальный префикс (наверняка валидный)
        row = cursor.query('select t.operator_id, min(p.prefix) min_prefix from operators t, prefix_anatel p '
                           'where t.operator_id = p.operator_id '
                           'group by t.operator_id '
                           'limit 1').fetchone()
    operator_id = row['operator_id']
    min_prefix = row['min_prefix']
    # добавим такой номер в систему (МОЖНО ЗАТЕРЕТЬ РЕЛЬНЫЕ ДАННЫЕ!)
    date_future = date_to_str(datetime.datetime.now())
    resp = client.put('/v1.0/phone_number/55%s' % min_prefix,
                      data=json.dumps({"operator_id": operator_id, "date": date_future}),
                      content_type='application/json')
    assert resp.status_code == 204
    # проверим содержимое посредством get
    resp = client.get('/v1.0/phone_number/55%s' % min_prefix)
    assert resp.status_code == 200
    json_data = to_json(resp.data)
    assert json_data['is_valid'] is True
    assert json_data['is_porting'] is True
    assert json_data['operator']['operator_id'] == operator_id
    assert json_data['porting_date'] == date_future
    # удаление номера
    resp = client.delete('/v1.0/phone_number/55%s' % min_prefix)
    assert resp.status_code == 204
    with get_cursor(commit=False) as cursor:
        cnt = cursor.query('select count(*) cnt from portable_number t where t.phone_number = %s',
                           (min_prefix,)).fetchone()['cnt']
        assert cnt == 0
