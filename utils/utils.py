"""
some utilities - logger, etc.
"""
import datetime
import logging


def get_logger(logger_name, level=logging.INFO):
    """

    :param logger_name:
    :return:
    """
    log = logging.getLogger(logger_name)
    log.setLevel(level)

    # TODO handler_file is empty!!!
    handler_stream = logging.StreamHandler()
    handler_file = logging.FileHandler('super_log')

    formatter = '%(asctime)s %(levelname)s - %(name)s : %(message)s'
    handler_stream.setFormatter(logging.Formatter(formatter))
    handler_file.setFormatter(logging.Formatter(formatter))

    log.addHandler(handler_stream)
    log.addHandler(handler_file)
    # log.setLevel(logging.getLogger().level)
    return log


def date_to_str(date, frmt='%Y-%m-%d %H:%M:%S'):
    """ convert date to string by format """
    return date.strftime(frmt)


def str_to_date(string, frmt='%Y-%m-%d %H:%M:%S'):
    """ convert string to date by format """
    return datetime.datetime.strptime(string, frmt)
